# README #


### Dragons of Mugloar solution ###


### Setup, running and output ###

For building and running tests, use `mvn clean install`. For running the application, use `mvn spring-boot:run`. 
In general, I inclined towards treating the application as command-line desktop 'script' application using the Spring REST client 
for accessing the Mugloar server. So, for example I opted to store and load configuration properties (see below) as I would in 
a Java SE application. The "interface" is quite simple: the output is log messages showing the 'progress' of the game, concluding
with whether the game was 'won' or 'lost', according to the (configurable) objective in amount of points, along with several 
other statistics of interest.

The messages in the output are what I considered of interest while developing and experimenting with the solution. In the end, 
I think they give a pretty good idea of the game progress, and served well in casual debugging and sanity testing. 
In the next sections, I am going to give a summary about the solution: how it works, its results, and more practically, how it 
can be parameterised.


### Solution description and parameterisation ###

The solution itself is quite simple: the available messages are queried from the server, and then sorted based on their 
respective "weighted" reward: the reward in gold multiplied by the estimated probability corresponding to the probability label.
To measure each estimate, a utility was written and each label is mapped in the application with a probability value. 
This utility class is the `com.bigdragons.util.Utility` class and is kept in the repository for "historical" reasons. 
A more sophisticated approach in a future version, for example, would be to keep track of live results in database or have a 
separate "research" application with the ability to configure the main application according to its results.

After sorting, the messages are selected based on a configurable threshold value. Messages that pass this threshold will 
be attempted to be solved. If no messages satisfy this threshold, then a configurable minimum number of messages will be tried,
before requesting new messages. The most seemingly costless trick would be to use a turn to ask for new messages, just in case
a better one is available. An obvious cost there would be missing out on potentially better messages that would expire, and 
also noticed that this behaviour is somewhat penalised, as solving performance in general goes down this way.

The facility I used for improving the solving performance is shopping for items. The best obvious item 
to buy is an extra life, which is also the cheapest one, but throwing money into blindly buying as many lives possible didn't 
prove really efficient. No serious investigation was conducted (like about the probabilities in the previous paragraph) about the 
suitability and function of each item, but I tried to parameterise the "shopping strategies" as much as possible and try out 
each one (more in the "results" section). For sure, different strategies as per the selection of items and the budgeting have 
been noticed to yield different results, so it seems as an improvement over only buying lives or "chance alone" strategies 
(ironically the "random" strategies are the ones which win, but more on that below).

This was a basic description of the solution. Below, an overview of the various configuration parameters mentioned is 
given:

`reward_threshold` - the minimum "weighted" reward that a message needs to have to be selected by default for solving

`min_cached_messages` - the minimum number of messages that will be cached for solving if no messages satisfying the minimum 
threshold requirements are found

`shopping_frequency` - the frequency in turns that shopping a new item for help will be attempted (as long as the necessary 
cost and budget constraints are a satisfied)

`objective` - the game objective in amount of points. If the objective is reached, the game stops as "won". 
If lives end before the objective is reached, the game is "over" and lost.

`money_saving_factor` and `money_saving_buffer` - these are parameters regarding the shopping budgeting. 
For "saving factor", the cost of the item must be below or equal to the gold available divided by the factor, and 
for "saving buffer" the gold available must be above or equal to the item's cost plus the buffer amount. 
Which one is used at any time is implemented in the code itself and it is not configurable as a property, so one of them will 
be ignored. The reason for this is that I didn't find significant differences in practice between the two, but at least some 
kind of budgeting must be there for protection. Check `com.bigdragons.ai.GameSolver.MONEY_AVAILABLE_CALCULATOR` for 
configuring the "budget strategy" through code.

`shopping_strategy` - essentially how the next item to buy is chosen, in a "greedy algorithm" style. The "strategies" supported
are: _NextItem_, _NextRandomItemSingleton_, _NextRandomItem_ and _NextRandomUnusedItem_. These will be discussed briefly in the
next section.

These were the game parameters. There are fallbacks in code for each of them missing or misconfigured, and they can be 
configured in the  `src/main/resources/application.properties` file. Right next, a discussion on the output and results 
follows.

### Output and results ###

Various statistics have been kept for determining game performance and tactics. The ones that are displayed on the final log 
message containing the game result, are: the final score achieved, the amount of gold left, the moves number (how many solving 
attempts were made) and the message updates number (how many times new messages were requested from the server). Finally, the 
success rate (successful to total attempts) is displayed.

These give a good summary of each game and helped to evaluate how the cards were dealt, so to speak, from the server 
for each game. For example, the ratio of message retrievals to attempts might be a good indicator of how good are the messages
being offered and comparing that with the success rate could also be an indicator of how the shopping 
strategy helped. However, this is just a casual observation at this point, to indicate how various statistics could be 
interpreted and analysed further.

About the shopping strategies, I won't go into much detail here: the _NextItem_ follows a sequence on "cost order" and assumes 
that buying one item once is enough (unless it is an "extra life" obviously which is treated as a special case anyway). This 
was seemingly the worse strategy than the other "randomized" ones. Seems like picking a shopping item randomly is better than
going in predetermined order by cost. The additional constraints of these strategies are described in the code javadoc, but 
pretty much they yield similar results.

Finally, last and most important, is the actual performance of the algorithm in general in terms of reliability. 
The "good" strategies seem to yield a percentage of at least 85% so far per game. For evaluation, other numbers I looked for 
are the median success rate and how much helpful the purchasing decisions were (as described in the previous paragraph). 
Generally, this result is something I am satisfied with at this point. A good experiment would be to try with even higher 
objectives and compare the various strategies under stress, as well as try out different parameters (although I think the 
defaults are good enough from what has been tried out so far).
