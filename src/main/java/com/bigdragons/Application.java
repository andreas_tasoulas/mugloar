package com.bigdragons;

import com.bigdragons.ai.GameSolver;
import com.bigdragons.client.Client;
import com.bigdragons.domain.Game;
import com.bigdragons.domain.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String [] args) {

        Client client = new Client();
        GameDTO gameDTO = client.startGame();

        assert gameDTO != null;
        Game game = new Game(gameDTO.getGameId());

        String gameResult = null;

        GameSolver gameSolver = new GameSolver(game, client);

        try {
            if (gameSolver.solveGame()) {
                gameResult = "Game won with score: " + game.getScore() + " and gold: " + game.getGold()
                        + " after " + game.getMoves() + " moves and " + game.getMessageUpdates() + " message updates"
                        + " - Success rate: " + game.getSuccessRate();
            } else {
                gameResult = "Game over with score: " + game.getScore() + " and gold: " + game.getGold()
                        + " after " + game.getMoves() + " moves and " + game.getMessageUpdates() + " message updates"
                        + " - Success rate: " + game.getSuccessRate();
            }
        } catch (Exception ex) {
             log.error("Unexpected error: " + ex.getMessage());
        }

        log.info("Game Result:");
        log.info(gameResult);
    }
}
