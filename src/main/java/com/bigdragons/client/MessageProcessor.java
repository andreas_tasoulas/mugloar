package com.bigdragons.client;

import com.bigdragons.domain.Message;
import com.bigdragons.domain.dto.MessageDTO;
import com.bigdragons.util.WeightedRewardCalculator;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MessageProcessor {

    private List<Message> messages = new ArrayList<>();
    private List<Message> selectedMessages = new ArrayList<>();

    private int rewardThreshold;
    private int minCachedMessages;

    public MessageProcessor(int rewardThreshold, int minCachedMessages, List<MessageDTO> messageDTOs) {
        this.rewardThreshold = rewardThreshold;
        this.minCachedMessages = minCachedMessages;
        loadSorted(messageDTOs);
    }

    public List<Message> getAvailableMessages() {
        return this.messages;
    }

    public List<Message> getSelectedMessages() {
        return this.selectedMessages;
    }

    public void decrementExpiry() {
        selectedMessages.forEach(Message::decrementExpiry);
    }

    /**
     * "Pop" the next message for solving.
     *
     *
     * @return the message id of the next message that will be attempted to be solved. If no remaining and not expired
     * messages are found returns null, which will be used as a criterion for retrieving new messages from the server.
     */
    public String popForSolving() {

        String messageId = null;

        Optional<Message> retVal =
                this.selectedMessages.stream().filter(message -> message.getExpiresIn() > 0).findFirst();

        if (retVal.isPresent()) {

            Message retMessage = retVal.get();

            messageId = retMessage.getAdId();
            this.selectedMessages.remove(retMessage);

            this.selectedMessages.forEach(Message::decrementExpiry);
        }

        return messageId;
    }

    private void selectMessagesForSolving() {

        this.selectedMessages =
                this.messages.stream().filter(message -> message.getWeightedReward().doubleValue() > rewardThreshold)
                        .collect(Collectors.toList());

        if (this.selectedMessages.isEmpty()) {
            IntStream.range(0, minCachedMessages).forEach(i -> this.selectedMessages.add(this.messages.get(i)));
        }
    }

    private void loadSorted(List<MessageDTO> messageDTOs) {
        clearMessages();
        messageDTOs.forEach(messageDTO -> {
            BigDecimal weightedReward = WeightedRewardCalculator.getWeightedReward(messageDTO);
            this.messages.add(new Message(messageDTO.getAdId(), messageDTO.getExpiresIn(), weightedReward));
        });
        Collections.sort(this.messages);
        selectMessagesForSolving();
    }

    private void clearMessages() {
        this.messages = new ArrayList<>();
        this.selectedMessages = new ArrayList<>();
    }
}
