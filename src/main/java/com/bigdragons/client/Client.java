package com.bigdragons.client;

import com.bigdragons.domain.Game;
import com.bigdragons.domain.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * This class comprises methods for accessing the Mugloar server
 */
public class Client {

    private static final Logger log = LoggerFactory.getLogger(Client.class);

    // This can also be a bad practice: adding a level of indirection when you want to search for endpoint urls
    private static final String BASE_URL = "https://dragonsofmugloar.com/api/v2/";
    private RestTemplate restTemplate = new RestTemplate();

    public GameDTO startGame() {
        return restTemplate.postForObject(BASE_URL + "game/start", null, GameDTO.class);
    }

    public ReputationDTO investigateReputation(String gameId) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("gameId", gameId);

        return restTemplate.postForObject(BASE_URL + "{gameId}/investigate/reputation",
                null, ReputationDTO.class, parameters);
    }

    public DomainDataCollection<MessageDTO> getMessages(Game game) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("gameId", game.getGameId());

        MessageDTO [] messages = restTemplate.getForObject(BASE_URL + "{gameId}/messages",
                MessageDTO[].class, parameters);

        game.addMessageUpdateCount();

        List<MessageDTO> messageDTOs = nonNull(messages) ? Arrays.asList(messages) : new ArrayList<>();

        return new DomainDataCollection<>(messageDTOs.stream()
                .collect(Collectors.toMap(MessageDTO::getAdId, messageDTO -> messageDTO)));
    }

    public DomainDataCollection<ShoppingItemDTO> getShoppingItems(String gameId) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("gameId", gameId);

        ShoppingItemDTO[] shoppingItems =
                restTemplate.getForObject(BASE_URL + "{gameId}/shop", ShoppingItemDTO[].class, parameters);

        List<ShoppingItemDTO> shoppingItemDTOs = nonNull(shoppingItems) ?
                Arrays.asList(shoppingItems) : new ArrayList<>();

        return new DomainDataCollection<>(shoppingItemDTOs.stream()
                .collect(Collectors.toMap(ShoppingItemDTO::getId, dto -> dto)));
    }

    public ResultDTO solve(Game game, String adId) {
        game.addMoveCount();
        return solve(game.getGameId(), adId);
    }

    public ResultDTO solve(String gameId, String adId) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("gameId", gameId);
        parameters.put("adId", adId);

        ResultDTO resultDTO = null;

        try {
            resultDTO = restTemplate.postForObject(BASE_URL + "{gameId}/solve/{adId}", null,
                    ResultDTO.class, parameters);
        } catch (RestClientException ex) {
            log.error("Error while trying to solve message: " + adId + ": " + ex.getMessage());
        }

        assert resultDTO != null;

        return resultDTO;
    }

    public ShoppingResultDTO buyItem(String gameId, String itemId) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("gameId", gameId);
        parameters.put("itemId", itemId);

        ShoppingResultDTO resultDTO = null;

        try {
            resultDTO = restTemplate.postForObject(BASE_URL + "{gameId}/shop/buy/{itemId}", null,
                    ShoppingResultDTO.class, parameters);
        } catch (RestClientException ex) {
            return null;
        }

        return resultDTO;
    }
}
