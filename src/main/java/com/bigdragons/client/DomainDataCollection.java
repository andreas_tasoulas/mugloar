package com.bigdragons.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DomainDataCollection<T> {

    private Map<String, T> domainItems;

    public DomainDataCollection(Map<String, T> domainItems) {
        this.domainItems = domainItems;
    }

    public void add(String key, T item) {
        this.domainItems.put(key, item);
    }

    public void remove(String key) {
        this.domainItems.remove(key);
    }

    public Map<String, T> getMap() {
        return this.domainItems;
    }

    public List<T> getValues() {
        return new ArrayList<>(domainItems.values());
    }

    public T getValue(String id) {
        return this.domainItems.get(id);
    }
}