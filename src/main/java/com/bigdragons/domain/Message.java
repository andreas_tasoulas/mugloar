package com.bigdragons.domain;

import java.math.BigDecimal;
import java.util.Objects;

public class Message implements Comparable<Message> {

    private String adId;
    private Integer expiresIn;
    private BigDecimal weightedReward;

    public Message(String adId, Integer expiresIn, BigDecimal weightedReward) {
        this.adId = adId;
        this.expiresIn = expiresIn;
        this.weightedReward = weightedReward;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message = (Message) o;
        return adId.equals(message.adId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adId);
    }

    @Override
    public String toString() {
        return this.adId + " message reward: " + this.weightedReward + " expiring in: " + this.expiresIn;
    }

    public String getAdId() {
        return this.adId;
    }

    public Integer getExpiresIn() {
        return this.expiresIn;
    }

    public BigDecimal getWeightedReward() {
        return this.weightedReward;
    }

    public void decrementExpiry() {
        this.expiresIn--;
    }

    @Override
    public int compareTo(Message message) {
        BigDecimal difference = message.weightedReward.subtract(weightedReward);
        return difference.intValue();
    }
}
