package com.bigdragons.domain.dto;

public class ShoppingItemDTO {

    private String id;
    private String name;
    private Integer cost;

    @Override
    public String toString() {
        return "id: " + this.id + ", name: " + this.name + ", cost: " + this.cost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
