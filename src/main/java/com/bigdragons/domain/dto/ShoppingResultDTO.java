package com.bigdragons.domain.dto;

public class ShoppingResultDTO {

    private String shoppingSuccess;
    private Integer gold;
    private Integer lives;
    private Double level;
    private Integer turn;

    @Override
    public String toString() {
        return "Purchase success: " + this.shoppingSuccess + ", lives: " + this.lives + ", gold: " + this.gold;
    }

    public String getShoppingSuccess() {
        return shoppingSuccess;
    }

    public boolean isSuccessful() {
        return Boolean.parseBoolean(shoppingSuccess);
    }

    public void setShoppingSuccess(String shoppingSuccess) {
        this.shoppingSuccess = shoppingSuccess;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Integer getLives() {
        return lives;
    }

    public void setLives(Integer lives) {
        this.lives = lives;
    }

    public Double getLevel() {
        return level;
    }

    public void setLevel(Double level) {
        this.level = level;
    }

    public Integer getTurn() {
        return turn;
    }

    public void setTurn(Integer turn) {
        this.turn = turn;
    }
}
