package com.bigdragons.domain.dto;

public class ReputationDTO {

    private Double people;
    private Double state;
    private Double underworld;

    @Override
    public String toString() {
        return "People: " + people + ", State: " + state + ", Underworld: " + underworld;
    }

    public Double getPeople() {
        return people;
    }

    public void setPeople(Double people) {
        this.people = people;
    }

    public Double getState() {
        return state;
    }

    public void setState(Double state) {
        this.state = state;
    }

    public Double getUnderworld() {
        return underworld;
    }

    public void setUnderworld(Double underworld) {
        this.underworld = underworld;
    }
}
