package com.bigdragons.domain;

import com.bigdragons.domain.dto.ResultDTO;
import com.bigdragons.domain.dto.ShoppingResultDTO;

public class Game {

    private String gameId;
    private Integer score;
    private Integer gold;
    private Integer lives;
    private Integer moves = 0;
    private Integer messageUpdates = 0;
    private Integer successCount = 0;

    public Game(String gameId) {
        this.gameId = gameId;
        this.score = 0;
        this.gold = 0;
        this.moves = 0;
        this.messageUpdates = 0;
        this.successCount = 0;
    }

    /**
     * Updates the game state after the latest solving attempt.
     *
     * @param result the latest solving attempt result dto
     */
    public void update(ResultDTO result) {
        this.score = result.getScore();
        this.gold = result.getGold();
        this.lives = result.getLives();
        if (result.isSuccess()) {
            addSuccessCount();
        }
    }

    public void addMoveCount() {
        ++moves;
    }

    private void addSuccessCount() {
        ++successCount;
    }

    public void addMessageUpdateCount() {
        ++messageUpdates;
    }

    public Integer getMoves() {
        return this.moves;
    }

    public Integer getMessageUpdates() {
        return this.messageUpdates;
    }

    public Double getSuccessRate() {
        return successCount.doubleValue() / moves;
    }

    public void update(ShoppingResultDTO shoppingResult) {
        this.gold = shoppingResult.getGold();
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getScore() {
        return this.score;
    }

    public Integer getLives() {
        return this.lives;
    }
}
