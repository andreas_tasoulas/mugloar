package com.bigdragons.ai;

import com.bigdragons.client.MessageProcessor;
import com.bigdragons.client.Client;
import com.bigdragons.client.DomainDataCollection;
import com.bigdragons.domain.Game;
import com.bigdragons.domain.dto.MessageDTO;
import com.bigdragons.domain.dto.ResultDTO;
import com.bigdragons.domain.dto.ShoppingItemDTO;
import com.bigdragons.domain.dto.ShoppingResultDTO;
import com.bigdragons.util.CryptUtil;
import com.bigdragons.util.PropertyLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;

public class GameSolver {

    private static final Logger log = LoggerFactory.getLogger(GameSolver.class);

    private Client client;
    private Game game;
    private MessageProcessor messageProcessor;
    private ShoppingStrategy shoppingStrategy;

    private int rewardThreshold;
    private int minCachedMessages;
    private int shoppingFrequency;
    private int objective;
    private static int moneySavingFactor;
    private static int moneySavingBuffer;

    static final Function<Integer, Integer> SAVE_WITH_BUFFER = (cost) -> cost + moneySavingBuffer;
    static final Function<Integer, Integer> SAVE_WITH_FACTOR = (cost) -> cost * moneySavingFactor;

    static final Function<Integer, Integer> MONEY_AVAILABLE_CALCULATOR = SAVE_WITH_FACTOR;

    public GameSolver(Game game, Client client) {

        this.rewardThreshold = (Integer) PropertyLoader.getPropertyValue("reward_threshold");
        this.minCachedMessages = (Integer) PropertyLoader.getPropertyValue("min_cached_messages");
        this.shoppingFrequency = (Integer) PropertyLoader.getPropertyValue("shopping_frequency");
        this.objective = (Integer) PropertyLoader.getPropertyValue("objective");

        moneySavingFactor = (Integer) PropertyLoader.getPropertyValue("money_saving_factor");
        moneySavingBuffer = (Integer) PropertyLoader.getPropertyValue("money_saving_buffer");

        this.shoppingStrategy = new ShoppingStrategy(MONEY_AVAILABLE_CALCULATOR,
                (String) PropertyLoader.getPropertyValue("shopping_strategy"));

        this.game = game;
        this.client = client;
    }

    /**
     *
     * Implements the "game loop" which ends when either the objective has been reached or the lives have ended.
     *
     * <p>In each a turn, the available messages are retrieved, sorted and the ones that will be tried are selected.
     * The number of messages depends on whether the message's weighted value (depending on probability and reward)
     * exceeds a predetermined threshold and if no messages satisfy this criterion, then a minimum number of default
     * messages are selected, based on their sorting.
     *
     *
     * @return <tt>true</tt> if game was 'won' (objective achieved)
     * or 'lost' (lives ending before objective was achieved)
     */
    public boolean solveGame() {

        while (game.getScore() < objective) {

            DomainDataCollection<MessageDTO> domainDataCollection = client.getMessages(game);
            CryptUtil.decryptEncryptedMessages(domainDataCollection);

            log.info("Available messages");
            domainDataCollection.getValues().forEach(messageDTO -> log.info(messageDTO.toString()));

            messageProcessor = new MessageProcessor(rewardThreshold, minCachedMessages,
                    new ArrayList<>(domainDataCollection.getValues()));

            String messageId = null;

            log.info("Available messages details after update");
            messageProcessor.getAvailableMessages().forEach(message -> log.info(message.toString()));

            log.info("Selected messages details after update");
            messageProcessor.getSelectedMessages().forEach(message -> log.info(message.toString()));

            while ((messageId = messageProcessor.popForSolving()) != null) {

                log.info("Solving: " + domainDataCollection.getValue(messageId).toString());
                ResultDTO result = client.solve(game, messageId);

                log.info("Result: " + result.toString() +"\n");
                game.update(result);

                log.info("Selected messages details");
                messageProcessor.getSelectedMessages().forEach(message -> log.info(message.toString()));

                if (result.getLives() == 0) {
                    return game.getScore() >= objective;
                }

                if (game.getLives() <= 1 || game.getMoves() % shoppingFrequency == 0) {
                    getHelp(result);
                }

                game.setScore(result.getScore());
            }
        }

        return true;
    }

    /**
     *
     * Initiates "getting help" flow which may end in buying some item, based on specific conditions.
     *
     * @param result the latest message solving attempt result dto
     */
    private void getHelp(ResultDTO result) {

        ShoppingItemDTO shoppingItem = shopHelp();

        assert shoppingItem != null;

        log.info("Current gold available: " + result.getGold() + " for shopping item " + shoppingItem.getId());
        if (buyingConditionsSatisfied(result.getGold(), shoppingItem)) {
            buyItem(shoppingItem.getId());
        }
    }

    /**
     *
     * Gets the available shopping items and decides which one to buy next.
     *
     * @return the dto with the details of the shopping item selected to buy, null if no item is available or suitable
     */
    private ShoppingItemDTO shopHelp() {

        try {

            DomainDataCollection<ShoppingItemDTO> shoppingItems = client.getShoppingItems(game.getGameId());

            String itemId = shoppingStrategy.suggestShoppingItem(game, shoppingItems.getMap());

            log.info("Available Shopping Items:");
            shoppingItems.getValues().forEach(item -> log.info(item.toString()));
            return shoppingItems.getValue(itemId);
        } catch (Exception ex) {
            log.error("Error while retrieving shopping items");
        }

        return null;
    }

    /**
     *
     * Checks if the purchasing decision satisfies the "budget strategy".
     *
     * @param gold money available
     * @param shoppingItem shopping item details
     * @return <tt>true</tt> if the purchasing decision falls within the budget
     */
    private boolean buyingConditionsSatisfied(Integer gold, ShoppingItemDTO shoppingItem) {
        return (ShoppingStrategy.HEALING_ID.equals(shoppingItem.getId()) && gold >= shoppingItem.getCost()) ||
                (gold >= MONEY_AVAILABLE_CALCULATOR.apply(shoppingItem.getCost()));
    }

    /**
     *
     * Buys the item, updating the game state accordingly.
     *
     * @param itemId the id of the item to buy
     */
    private void buyItem(String itemId) {
        ShoppingResultDTO shoppingResult = client.buyItem(game.getGameId(), itemId);
        assert shoppingResult != null;
        log.info(shoppingResult.toString() + " of item " + itemId + "\n");
        game.update(shoppingResult);
        shoppingStrategy.updateStatus(shoppingResult, itemId);
        messageProcessor.decrementExpiry();
    }
}
