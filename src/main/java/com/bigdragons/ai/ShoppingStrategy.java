package com.bigdragons.ai;

import com.bigdragons.domain.Game;
import com.bigdragons.domain.dto.ShoppingItemDTO;
import com.bigdragons.domain.dto.ShoppingResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ShoppingStrategy {

    private static final Logger log = LoggerFactory.getLogger(ShoppingStrategy.class);

    public static final String HEALING_ID = "hpot";

    // hpot, cs, gas, wax, tricks, wingpot, ch, rf, iron, mtrix, wingpotmax
    private static final String DEFAULT_ITEM = "hpot";

    private static Map<String, Boolean> itemStock = new HashMap<>();
    private static List<String> items = Arrays
            .asList("cs", "gas", "wax", "tricks", "wingpot", "ch", "rf", "iron", "mtrix", "wingpotmax");

    static {
        items.forEach(item -> itemStock.put(item, false));
    }

    private int currentItemIndex = 0;

    private Integer gold;
    private Map<String, ShoppingItemDTO> shoppingItems;
    private Function<Integer, Integer> moneyAvailableCalculator;

    private final Supplier<String> DEFAULT_STRATEGY = this::getNextRandomUnusedItem;

    private Supplier<String> nextItemSupplier;

    private Map<String, Supplier<String>> availableStrategies;

    public ShoppingStrategy(Function<Integer, Integer> moneyAvailableCalculator, String shoppingStrategy)  {

        loadAvailableStrategies();

        this.moneyAvailableCalculator = moneyAvailableCalculator;
        this.nextItemSupplier = availableStrategies.get(shoppingStrategy);

        if (this.nextItemSupplier == null) {
            log.info("Configuration strategy not found. Falling back to default strategy.");
            this.nextItemSupplier = DEFAULT_STRATEGY;
        }
    }

    private void loadAvailableStrategies() {
        availableStrategies = new HashMap<>();
        availableStrategies.put("NextItem", this::getNextItem);
        availableStrategies.put("NextRandomItemSingleton", this::getNextRandomItemSingleton);
        availableStrategies.put("NextRandomItem", this::getNextRandomItem);
        availableStrategies.put("NextRandomUnusedItem", this::getNextRandomUnusedItem);
    }

    public void updateStatus(ShoppingResultDTO shoppingResult, String itemId) {
        if (shoppingResult.isSuccessful() && !HEALING_ID.equals(itemId)) {
            ++currentItemIndex;
        }
    }

    /**
     *
     * Suggest the next shopping item to buy, based on a pre-configured "next-item" strategy
     * (greedy algorithm parameterization, with a function for selecting the next item as parameter).
     *
     * @param game the game details
     * @param shoppingItems shopping items available for buying
     * @return id of the next item to buy
     */
    public String suggestShoppingItem(Game game, Map<String, ShoppingItemDTO> shoppingItems) {

        this.gold = game.getGold();
        this.shoppingItems = shoppingItems;

        if (game.getLives() <= 1) {
            return HEALING_ID;
        } else {
            return nextItemSupplier.get();
        }
    }

    private static Random rnd = new Random();

    private <T> T getRandom(List<T> list) {
        return list.get(rnd.nextInt(list.size()));
    }

    /**
     *
     * One of the methods implementing "buy next" strategies.
     *
     * @return the next item id in "cost order", which hasn't been bought already.
     * Buy the "default" item (typically a "life" item) if all items have been bought once.
     */
    private String getNextItem() {

        if (currentItemIndex >= items.size()) {
            return getDefaultItem();
        }

        return items.get(currentItemIndex);
    }

    /**
     *
     * One of the methods implementing "buy next" strategies.
     *
     * @return a random item id that hasn't been bought already
     */
    private String getNextRandomItemSingleton() {

        List<String> selectedItems = getEligibleShoppingItems();

        if (selectedItems.isEmpty()) {
            return getDefaultItem();
        }

        String itemId = getRandom(selectedItems);

        itemStock.put(itemId, true);
        return itemId;
    }

    /**
     * One of the methods implementing "buy next" strategies. Ensures that an item is not bought more than once unless
     * all other affordable items have been tried at least once.
     *
     * @return a random item id from those (candidate items at the time) which haven't been bought so far
     */
    private String getNextRandomUnusedItem() {

        List<String> itemPool;
        List<String> selectedItems = getEligibleShoppingItems();

        if (selectedItems.isEmpty()) {
            itemPool = getAffordableShoppingItems();
        } else {
            itemPool = selectedItems;
        }

        if (itemPool.isEmpty()) {
            return getDefaultItem();
        }

        String itemId = getRandom(itemPool);

        itemStock.put(itemId, true);
        return itemId;
    }

    /**
     *
     * One of the methods implementing "buy next" strategies.
     *
     * @return the id of a random (affordable) item
     */
    // buy multiple random items without any restrictions
    private String getNextRandomItem() {

        List<String> items = getAffordableShoppingItems();

        if (items.isEmpty()) {
            return getDefaultItem();
        }

        return getRandom(items);
    }

    /**
     *
     * Gets all the affordable items that haven't been bought before
     *
     * @return a list of all item ids that are eligible based on cost, budget and constraints by specific algorithms.
     */
    private List<String> getEligibleShoppingItems() {
        return itemStock.entrySet()
                .stream().filter(entry -> !entry.getValue()
                        && gold >= moneyAvailableCalculator.apply(shoppingItems.get(entry.getKey()).getCost()))
                .map(Map.Entry::getKey).collect(Collectors.toList());
    }

    /**
     *
     * Gets all the affordable items.
     * Example budget strategies are having a buffer of money above the candidate item cost or a multiple of this cost.
     *
     * @return a list of all item ids that are affordable, based on a budget strategy
     */
    private List<String> getAffordableShoppingItems() {
        return items.stream()
                .filter(item -> gold >= moneyAvailableCalculator.apply(shoppingItems.get(item).getCost()))
                .collect(Collectors.toList());
    }

    private String getDefaultItem() {
        return DEFAULT_ITEM;
    }
}
