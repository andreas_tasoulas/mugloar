package com.bigdragons.util;

public class Probability {

    private String label;
    private Integer success = 0;
    private Integer attempts = 0;

    public Probability(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.success + "/" + this.attempts;
    }

    public void register(Boolean successResult) {
        ++attempts;
        if (successResult) ++success;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getAttempts() {
        return attempts;
    }

    public void setAttempts(Integer attempts) {
        this.attempts = attempts;
    }
}
