package com.bigdragons.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ProbabilityMap {

    private Map<String, Probability> probabilityMap = new HashMap<>();

    @Override
    public String toString() {

        StringBuilder probabilityMapStringBuilder = new StringBuilder();

        probabilityMapStringBuilder.append("\n");
        probabilityMap.forEach((k, v) -> {
            probabilityMapStringBuilder.append("Label: ").append(k).append(", Result: ").append(v).append("\n");
        });

        return probabilityMapStringBuilder.toString();
    }

    public void addProbabilityIfAbsent(String probabilityLabel) {
        probabilityMap.putIfAbsent(probabilityLabel, new Probability(probabilityLabel));
    }

    public Set<String> getLabels() {
        return this.probabilityMap.keySet();
    }

    public void register(String label, Boolean success) {

        // Initialisation on-demand when looking for specific probability (not polluting with mass initialisation)
        addProbabilityIfAbsent(label);

        probabilityMap.get(label).register(success);
    }
}
