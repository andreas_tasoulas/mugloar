package com.bigdragons.util;

import com.bigdragons.client.Client;
import com.bigdragons.domain.Game;
import com.bigdragons.domain.dto.GameDTO;
import com.bigdragons.domain.dto.MessageDTO;
import com.bigdragons.domain.dto.ResultDTO;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utility {

    private static final Logger log = LoggerFactory.getLogger(Utility.class);

    private static Client client = new Client();
    private static ProbabilityMap probabilityMap = new ProbabilityMap();

    private static Map<String, List<MessageDTO>> startupEvaluation(boolean multiProbabilityEvaluation) {

        GameDTO gameDTO = client.startGame();
        String gameId = gameDTO.getGameId();

        List<MessageDTO> messageDTOs = client.getMessages(new Game(gameId)).getValues();
        if (multiProbabilityEvaluation) {
            messageDTOs.forEach(messageDTO -> probabilityMap.addProbabilityIfAbsent(messageDTO.getProbability()));
        }

        return Collections.singletonMap(gameId, messageDTOs);
    }

    public static void evaluateProbability(String probabilityLabel) {

        Map<String, List<MessageDTO>> gameMessages = startupEvaluation(!nonNull(probabilityLabel));

        String gameId = gameMessages.keySet().iterator().next();
        List<MessageDTO> messageDTOs = gameMessages.values().iterator().next();

        try {
            if (nonNull(probabilityLabel)) {
                evaluateLabel(gameId, messageDTOs, probabilityLabel);
            } else { // Generic case
                probabilityMap.getLabels().forEach(label -> {
                    evaluateLabel(gameId, messageDTOs, label);
                });
            }
        } finally {
            log.info("Probability map");
            log.info(probabilityMap.toString());
        }
    }

    private static boolean solveForSuccessAndPopularity(List<MessageDTO> messageDTOs, String gameId) {

        log.info("Clearing popular probabilities");

        Set<String> popularAndSuccessful = new HashSet<>(Arrays.asList("Sure thing", "Piece of cake", "Walk in the park"));

        List<MessageDTO> popularProbabilityMessages = messageDTOs.stream().filter(messageDTO -> popularAndSuccessful
                .contains(messageDTO.getProbability()))
                .collect(Collectors.toList());

        if (popularProbabilityMessages.isEmpty()) return false;

        popularProbabilityMessages.forEach(messageDTO -> {
                    client.solve(gameId, messageDTO.getAdId());
                });

        return true;
    }

    private static void evaluateLabel(String gameId, List<MessageDTO> messageDTOs, String label) {

        log.info("Evaluating probability label: " + label);

        Set<MessageDTO> filteredByLabelMessageDTOs =
                messageDTOs.stream().filter(messageDTO -> messageDTO.getProbability().equals(label))
                        .collect(Collectors.toSet());

        if (filteredByLabelMessageDTOs.isEmpty()) {
            if (solveForSuccessAndPopularity(messageDTOs, gameId)) {
                evaluateLabel(gameId, client.getMessages(new Game(gameId)).getValues(), label);
            } else {
                log.info("No popular messages found for moving to next rounds");
            }
        }

        filteredByLabelMessageDTOs.forEach(messageDTO -> {

            log.info("Solving: " + messageDTO.getAdId() + " of probability " + label);

            ResultDTO result = client.solve(gameId, messageDTO.getAdId());

            if (nonNull(result)) {
                probabilityMap.register(label, result.isSuccess());
                log.info(result.toString());
            }
        });

        // TODO this has advantages and disadvantages: continue looking until game is over
        // solveForSuccessAndPopularity(messageDTOs, gameId);
        // evaluateLabel(gameId, client.getMessages(gameId), label);
    }
}
