package com.bigdragons.util;

import com.bigdragons.domain.dto.MessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class WeightedRewardCalculator {

    private static Map<String, Double> probability;

    private static final Logger log = LoggerFactory.getLogger(WeightedRewardCalculator.class);

    static {
        probability = new HashMap<>();
        probability.put("Sure thing", 0.99);
        probability.put("Piece of cake", 0.95);
        probability.put("Walk in the park", 0.9);
        probability.put("Quite likely", 0.8);
        probability.put("Hmmm....", 0.67);
        probability.put("Gamble", 0.67);
        probability.put("Risky", 0.5);
        probability.put("Suicide mission", 0.17);
        probability.put("Rather detrimental", 0.33);
        probability.put("Playing with fire", 0.33);
        probability.put("Impossible", 0.01);
    }

    public static BigDecimal getWeightedReward(MessageDTO messageDTO) {

        Double probabilityValue = probability.get(messageDTO.getProbability());

        if (probabilityValue == null) {
            log.error("Unmapped probability: " + messageDTO.getProbability());
            return BigDecimal.ZERO;
        }

        return BigDecimal.valueOf(probabilityValue).multiply(BigDecimal.valueOf(messageDTO.getReward()));
    }
}
