package com.bigdragons.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static java.util.Objects.nonNull;

public class PropertyLoader {

    private static final Logger log = LoggerFactory.getLogger(PropertyLoader.class);

    private static Map<String, Object> propertiesMap = new HashMap<>();
    private static Properties applicationProperties = new Properties();
    private static Map<String, Integer> defaultPropertyValues = new HashMap<>();

    private static void loadFromFile(String property) {
        try {
            propertiesMap.put(property, Integer.parseInt(applicationProperties.getProperty(property)));
        } catch (NumberFormatException ex) {
            if (nonNull(applicationProperties.getProperty(property))) {
                log.error("Invalid format for property: " + property + ". It must be a number (integer)");
            }
        }
    }

    private static void loadDefaultIfMissing() {
        defaultPropertyValues.keySet().forEach(property -> {
            if (propertiesMap.get(property) == null) {
                log.info("Value not found or invalid for property: " + property + ". Loading default value");
                propertiesMap.put(property, defaultPropertyValues.get(property));
            }
        });
    }

    static {

        defaultPropertyValues.put("reward_threshold", 25);
        defaultPropertyValues.put("min_cached_messages", 2);
        defaultPropertyValues.put("shopping_frequency", 3);
        defaultPropertyValues.put("objective", 1000);
        defaultPropertyValues.put("money_saving_factor", 2);
        defaultPropertyValues.put("money_saving_buffer", 100);

        try {

            FileInputStream in = new FileInputStream("src/main/resources/application.properties");
            applicationProperties.load(in);

            defaultPropertyValues.keySet().forEach(PropertyLoader::loadFromFile);

            loadDefaultIfMissing();

            propertiesMap.put("shopping_strategy", applicationProperties.getProperty("shopping_strategy"));

        } catch (FileNotFoundException ex) {
            log.error("Properties file not found");
        } catch (IOException ex) {
            log.error("Error reading properties file");
        } catch (Exception ex) {
            log.error("Unexpected error: " + ex.getMessage());
        }
    }

    public static Object getPropertyValue(String property) {
        return propertiesMap.get(property);
    }
}
