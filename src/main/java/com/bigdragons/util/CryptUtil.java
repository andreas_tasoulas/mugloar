package com.bigdragons.util;

import com.bigdragons.Application;
import com.bigdragons.client.DomainDataCollection;
import com.bigdragons.domain.dto.MessageDTO;

import java.util.*;

import static java.util.Objects.nonNull;

public class CryptUtil {

    /**
     * Decode a message dto's fields using Base64
     *
     * @param messageDTO the message dto to be decoded
     * @return decoded message dto
     */
    private static MessageDTO decrypt(MessageDTO messageDTO) {

        byte [] adIdBytes = Base64.getDecoder().decode(messageDTO.getAdId());
        byte [] messageBytes = Base64.getDecoder().decode(messageDTO.getMessage());
        byte [] probabilityBytes = Base64.getDecoder().decode(messageDTO.getProbability());

        messageDTO.setAdId(new String(adIdBytes));
        messageDTO.setMessage(new String(messageBytes));
        messageDTO.setProbability(new String(probabilityBytes));
        messageDTO.setEncrypted(0);

        return messageDTO;
    }

    /**
     *
     * Decode a String value using Base64
     *
     * @param key the <tt>String</tt> value to be decoded. Typically, it is our message's key
     * @return the decoded value
     */
    private static String decrypt(String key) {
        return new String(Base64.getDecoder().decode(key));
    }

    /**
     * Decodes encoded messages in a collection and update their encoded fields with their decoded values.
     *
     * @param domainDataCollection a collection of messages to be decoded, from those that are 'encrypted'
     */
    public static void decryptEncryptedMessages(DomainDataCollection<MessageDTO> domainDataCollection) {

        Map<String, MessageDTO> decryptedMessages = new HashMap<>();
        Set<String> encryptedKeys = new HashSet<>();

        domainDataCollection.getMap().forEach((key, messageDTO)-> {
            if (nonNull(messageDTO.getEncrypted()) && messageDTO.getEncrypted() == 1) {
                decryptedMessages.put(decrypt(key), decrypt(messageDTO));
                encryptedKeys.add(key);
            }
        });

        encryptedKeys.forEach(domainDataCollection::remove);
        decryptedMessages.forEach(domainDataCollection::add);
    }
}
