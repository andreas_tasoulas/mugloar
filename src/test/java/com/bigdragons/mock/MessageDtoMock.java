package com.bigdragons.mock;

import com.bigdragons.domain.dto.MessageDTO;

public class MessageDtoMock extends MessageDTO {

    public MessageDtoMock(String key, int reward, String probability, int expiresIn) {
        this.setAdId(key);
        this.setReward(reward);
        this.setProbability(probability);
        this.setExpiresIn(expiresIn);
    }
}
