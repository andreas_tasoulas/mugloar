package com.bigdragons.client;

import com.bigdragons.domain.Message;
import com.bigdragons.domain.dto.MessageDTO;
import com.bigdragons.mock.MessageDtoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class MessageProcessorTest {

    private MessageProcessor messageProcessor;

    private List<MessageDTO> aboveThreshold;
    private List<MessageDTO> belowThreshold;

    @Before
    public void setUp() {

        aboveThreshold = Arrays.asList(
                new MessageDtoMock("gfm", 23, "Walk in the park", 7),
                new MessageDtoMock("5oP", 17, "Piece of cake",7),
                new MessageDtoMock("eRZ", 8, "Piece of cake", 7),
                new MessageDtoMock("XwK", 20, "Piece of cake",7 ),
                new MessageDtoMock("c80", 31, "Quite likely", 7),
                new MessageDtoMock("Oht", 41, "Piece of cake", 7),
                new MessageDtoMock("JU7", 20, "Quite likely", 7),
                new MessageDtoMock("pVV", 25, "Piece of cake", 7),
                new MessageDtoMock("ILm", 7, "Walk in the park", 7),
                new MessageDtoMock("wKS", 12, "Piece of cake", 7)
        );

        belowThreshold = Arrays.asList(
                new MessageDtoMock("gfm", 23, "Walk in the park", 7),
                new MessageDtoMock("5oP", 17, "Piece of cake", 7),
                new MessageDtoMock("eRZ", 8, "Piece of cake", 7),
                new MessageDtoMock("XwK", 20, "Piece of cake", 7),
                new MessageDtoMock("c80", 31, "Quite likely", 7),
                new MessageDtoMock("Oht", 14, "Piece of cake", 7),
                new MessageDtoMock("JU7", 20, "Quite likely", 7),
                new MessageDtoMock("pVV", 25, "Piece of cake", 1),
                new MessageDtoMock("ILm", 7, "Walk in the park", 7),
                new MessageDtoMock("wKS", 12, "Piece of cake", 7)
        );
    }

    @Test
    public void testPopForSolving() {

        this.messageProcessor = new MessageProcessor(25, 2, belowThreshold);

        assertEquals(2, this.messageProcessor.getSelectedMessages().size());

        String message = this.messageProcessor.popForSolving();
        assertEquals("c80", message);

        message = this.messageProcessor.popForSolving();
        assertNull(message);

        assertEquals(1, this.messageProcessor.getSelectedMessages().size());
        assertFalse(this.messageProcessor.getSelectedMessages().stream().map(Message::getAdId)
                .collect(Collectors.toSet()).contains("c80"));
    }

    @Test
    public void testSelectMessagesForSolvingAboveThreshold() {

        this.messageProcessor = new MessageProcessor(25, 2, aboveThreshold);

        assertEquals(1, this.messageProcessor.getSelectedMessages().size());
        assertEquals("Oht", this.messageProcessor.getSelectedMessages().get(0).getAdId());
    }

    @Test
    public void testSelectMessagesForSolvingBelowThreshold() {

        this.messageProcessor = new MessageProcessor(25, 2, belowThreshold);

        assertEquals(2, this.messageProcessor.getSelectedMessages().size());
        assertEquals("c80", this.messageProcessor.getSelectedMessages().get(0).getAdId());
        assertEquals("pVV", this.messageProcessor.getSelectedMessages().get(1).getAdId());
    }
}
